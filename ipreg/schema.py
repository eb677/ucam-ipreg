from dataclasses import dataclass
from datetime import date
from ipaddress import IPv4Address, IPv6Address
from typing import MutableSet, Union


__all__ = [
    "BaseObject",
    "BaseBox",
    "Box",
    "Vbox",
    "CNAME",
    "ANAME",
]


IPAddress = Union[IPv4Address, IPv6Address]


@dataclass
class BaseObject:
    name: str
    remarks: str
    review_date: date


@dataclass
class BaseBox(BaseObject):
    sysadmin: str
    end_user: str
    addresses: MutableSet[IPAddress]


@dataclass
class Box(BaseBox):
    equipment: str
    location: str
    owner: str


@dataclass
class Vbox(BaseBox):
    purpose: str
    host_box: Box


@dataclass
class CNAME(BaseObject):
    target_name: str


@dataclass
class ANAME(BaseObject):
    addresses: MutableSet[IPAddress]
