import ipaddress


__all__ = [
    "addresses_from_csv",
]


def addresses_from_csv(csvlist: str):
    return frozenset(ipaddress.ip_address(i) for i in csvlist.split(","))
