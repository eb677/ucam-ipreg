from dataclasses import dataclass
from urllib.parse import urljoin

import requests

from .exceptions import APIError

__all__ = [
    "BaseClient",
    "ClientV2",
]


URL = str


@dataclass
class BaseClient:
    """\
    Carries common functions for clients for all IPReg API versions.
    """

    base: URL

    def _endpoint(self, resource: str) -> URL:
        return urljoin(self.base, resource)

    def _get(self, resource: str, *args, **kwargs) -> requests.Response:
        url = self._endpoint(resource)
        kwargs["url"] = url
        with requests.get(*args, **kwargs) as resp:
            if resp.status_code != 200:
                raise APIError(url)
            else:
                return resp


@dataclass
class ClientV2(BaseClient):
    """\
    Client for the UIS IP Register database web API version 2.
    """

    cookie_value: str
    BASE_URL: URL = "https://jackdaw.cam.ac.uk/ipreg/"

    def _get(self, *args, **kwargs) -> requests.Response:
        kwargs["cookies"] = {"WebDBI_jackdaw": self.cookie_value}
        return super()._get(*args, **kwargs)
