class APIError(Exception):
    """
    Generic IP Register API error.
    """
